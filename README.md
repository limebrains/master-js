# master-js

Repository with materials for learning front-end developement - HTML, CSS, JavaScript, React, React-Native, Vue. Order of learning is important.

## HTML + CSS

[HTML5 + CSS3 for beginners](https://www.udemy.com/modern-web-design-html5-css3-beginners-guide-to-websites/)

[Sass - CSS pre-processor](https://www.udemy.com/sass-from-beginner-to-advanced/)

[CSS flex exercises](http://flexboxfroggy.com/)

## JavaScript
[(Documentation)](https://devdocs.io/javascript/)

[Mozilla - Learn JS](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)

[JavaScript crashcourse](https://learnxinyminutes.com/docs/javascript/)

[JavaScript exercises](http://www.learn-js.org/)

[Getting started with JavaScript](http://jstherightway.org/#getting-started)

[ES6 JavaScript essentials](https://www.udemy.com/essentials-in-javascript-es6/)

[Modern JavaScript cheatsheet](https://mbeaudru.github.io/modern-js-cheatsheet/)

[Boolean equality table](https://dorey.github.io/JavaScript-Equality-Table/)

[CheckiO - Solve programming challenges using JavaScript to become more comfortable with the language](https://checkio.org/)

## React
[(Documentation)](https://reactjs.org/docs/getting-started.html)

[React + Redux course](https://www.udemy.com/react-redux/)

## React-Native
[(Documentation)](https://facebook.github.io/react-native/docs/getting-started)

[React-Native + Redux course](https://www.udemy.com/the-complete-react-native-and-redux-course/)

## Vue
[(Documentation)](https://vuejs.org/v2/guide/)

[Vue2 - complete course](https://www.udemy.com/course/vuejs-2-the-complete-guide/)

## Articles worth reading

https://www.quirksmode.org/js/events_order.html

https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript

http://superherojs.com/#language
